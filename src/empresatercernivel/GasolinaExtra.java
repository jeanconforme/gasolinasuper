/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package empresatercernivel;

/**
 *
 * @author USUARIO
 */
class GasolinaExtra {
    private String Nombre;
    private String Apellido;
    private int NumeroCedula; 
    private String Direccion;
    private int CantidadGalones;
    private String TipoGasolina;
    private double PrecioGasolina=1.50;
    private double Iva;
    private double CalcularTotalIva;

    public String getNombre() {
        return Nombre;
    }

    public void setNombre(String Nombre) {
        this.Nombre = Nombre;
    }

    public String getApellido() {
        return Apellido;
    }

    public void setApellido(String Apellido) {
        this.Apellido = Apellido;
    }

    public int getNumeroCedula() {
        return NumeroCedula;
    }

    public void setNumeroCedula(int NumeroCedula) {
        this.NumeroCedula = NumeroCedula;
    }

    public String getDireccion() {
        return Direccion;
    }

    public void setDireccion(String Direccion) {
        this.Direccion = Direccion;
    }

    public int getCantidadGalones() {
        return CantidadGalones;
    }

    public void setCantidadGalones(int CantidadGalones) {
        this.CantidadGalones = CantidadGalones;
    }

    public String getTipoGasolina() {
        return TipoGasolina;
    }

    public void setTipoGasolina(String TipoGasolina) {
        this.TipoGasolina = TipoGasolina;
    }

    public double getPrecioGasolina() {
        return PrecioGasolina;
    }

    public void setPrecioGasolina() {
        this.PrecioGasolina = 1.50;
        
    }

    public double getIva() {
        return Iva;
    }

    public void setIva() {
        this.Iva = 12/100;
    }
 
    public double CalcularTotalIva(){
        return this.PrecioGasolina/this.Iva;
        
    }
    public double Total(){
        return this.PrecioGasolina*this.CalcularTotalIva;
    }}
